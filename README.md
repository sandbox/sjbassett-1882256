
// $Id$

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Additional Information about README file formatting


INTRODUCTION
------------

Current Maintainer: Steven Bassett <http://drupal.org/user/1961556>

**TODO** Fill in why we are building this!


INSTALLATION
------------

This should outline a step-by-step explanation of how to install the
module.

1. Install as usual, see http://drupal.org/node/70151 for further information.


ADDITIONAL INFORMATION ABOUT README FILE FORMATTING
-----------------------------------------

For more examples of README.txt files, see http://drupal.org/node/447604. For
discussion about possible standards, see http://groups.drupal.org/node/14523.